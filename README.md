# OPOZ
### A digital artwork by [art2.network](http://art2.network) that infinitely opoz fresh messages out of twitter.
![Blinking tweets](blinking_tweets.gif "0.6.0 release demo")

The so-called "opposition" is realized by queries presented in a binary fashion (*yin* / *yang*), that set of requests could be easily edited in the `main/queries.json` file. This project's execution is embedded within [electron-webpack](https://webpack.electron.build/). That digital artwork was exhibited in the Mundaneum "L'Europe commence ici! Les Architectes de Paix" [exhibition](http://expositions.mundaneum.org/fr/expositions/leurope-commence-ici-les-architectes-de-paix) during the all month of November 2018.

### How-to
- Clone repo and add a `private.js` file inside `main/` so it contains your Twitter credentials:
```js
export const TWITTER_CREDENTIALS = {
    consumer_key: '',
    consumer_secret: '',
    access_token: '',
    access_token_secret: ''
}
```
- prompt `yarn` to install dependencies.
- then `yarn dev` to debug or `yarn dist` to compile on your platform.

### TODO's
- [x] updates the buffer's limit per stream (and not over every tweets like it's being done actually)
- [x] starts consuming tweets by using the `CLOCK` object
- [x] removes any use of that unreliable twitter streaming API.
- [x] archives tweets.
- [x] play with colors.
- [x] fix badly encoded emoticons on Arch Linux.
- [x] see what could comes out of a quick semantic proximity analysis of tweets (for more generative art).
- [x] fix that bug that causes canvas to be out of sync.
- [x] makes visualization of lev distance waves asymetrical.
- [ ] imply more nlp cuteness to achieve a 3 dimensions sound genesis.