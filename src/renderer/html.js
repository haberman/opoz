import { remote } from 'electron'
import { toImage } from 'emojione'

export const GLOBAL_MARGIN = 20

/** Returns a list of tuples whith each word of the incoming `text`is associted with a detected type. */
export const words_encode = (text, tracks) => {
    const re = /(\S)+/g
    const words = []

    let match
    do {
        match = re.exec(text)
        if (match) {
            const word = { value: match[0], type: 'text' }

            const query_match = new RegExp(`${tracks.join('|')}`, 'gi').exec(match[0])
            if (query_match) { word.type = 'query' }
            else if (match[0].includes('@')) { word.type = 'mention' }
            else if (match[0].includes('#')) { word.type = 'hashtag' }
            else if (match[0].includes('http')) { word.type = 'url' }

            words.push(word)
        }
    } while (match)

    return words
}

// when updating, tells if the appended tweet should be inverted, consequently switching the stack.
let inverted = false

/** 
 Updates the `bottom` value of every visible tweets; also switches around the `inverted` class.
 Returns an array of ids of tweets being offset the visible screen.
*/
export const display_update = () => {
    const tweets = Array.from(document.querySelectorAll(`div.tweet`)).reverse()

    const offset_ids = []
    if (tweets.length <= 1) { return offset_ids }

    let bottom = GLOBAL_MARGIN
    tweets.forEach(el => {
        el.style.bottom = `${bottom}px`

        if (bottom - el.offsetHeight > remote.getCurrentWindow().getBounds().height) {
            offset_ids.push(el.getAttribute('data-id'))
        }

        if (inverted) { el.classList.add('inverted') }
        else { el.classList.remove('inverted') }

        bottom += el.offsetHeight + GLOBAL_MARGIN * 2
    })

    return offset_ids
}

/** Appends the visual DOM representaton of a tweet. */
export const tweet_create = tweet => {
    const container = document.createElement('div')
    container.setAttribute('data-id', tweet.id)
    container.classList.add('tweet', tweet.side)
    if (inverted) { container.classList.add('inverted') }

    const side_align = tweet.side === 'yin' ? 'left' : 'right'
    container.setAttribute('style', `${side_align}:${GLOBAL_MARGIN}px;bottom:${GLOBAL_MARGIN * 2}px`)

    let html = ''
    words_encode(tweet.text, tweet.tracks).forEach(
        word => {
            if (word.type === 'url') { return }
            else if (word.type !== 'text') { html += `<span class='entity-${word.type}'>${word.value}</span> ` }
            else { html += `${word.value} ` }
        })
    html = toImage(html)

    const date_str = new Date(parseInt(tweet.date_ms))
        .toLocaleString(undefined, {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
        })

    const info = document.createElement('div')
    info.classList.add('info')
    info.innerHTML = `<span class='info-date'>${date_str}</span>` // @TODO add author // RT?

    const content = document.createElement('p')
    content.innerHTML = html

    container.appendChild(info)
    container.appendChild(content)
    document.getElementById('app').appendChild(container)

    inverted = !inverted

    return container
}