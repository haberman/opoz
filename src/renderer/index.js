import './index'
import './app'

import { ipcRenderer } from 'electron'
import { LevenshteinDistance } from 'natural'

import * as html from './html'

import { TWEET_STATE } from '../common/states'
import { tweet_is_unique, history_append } from '../common/utils'

let state = 'idle'  // ready, running
let buffer = []     // in memory history of tweets'
let active_side = Math.random() > .5 ? 'yin' : 'yang'

let queue_full = false
let queue_renewing = false

const CLOCK = { start: null, timeout: Infinity, cycle_count: 0 }

const LETTER_MS = 30        // amount of ms it takes to read a letter
const MIN_COUPLE_BUFFER = 10 // minimum amount of couples until we consider the buffer to be low


/** Appends a unique tweet to `buffer`. */
const buffer_append = tweet => {
    if (buffer.length === 0 || tweet_is_unique(tweet, buffer)) { buffer.push(tweet) }
}

/** Sorts buffer chronologically. */
const buffer_sort = side => buffer.filter(t => t.side === side).sort((a, b) => a.date_ms < b.date_ms)

/** Find the buffered tweet by its id. */
const buffer_tweet = id => buffer.find(t => t.id == id)

/** Formats raw content of tweets' metadata with extended text if status is truncated. */
const tweet_format = tweet => {
    const res = {
        id: tweet.id,
        lang: tweet.lang,
        side: tweet.side,
        tracks: tweet.tracks,
        type: 'tweet',
        state: tweet.state
    }

    let status = tweet
    if (tweet.hasOwnProperty('quoted_status')) {
        res.type = 'quote'
        status = tweet.quoted_status
    } else if (tweet.hasOwnProperty('retweeted_status')) {
        res.type = 'retweet'
        status = tweet.retweeted_status
    }

    if (tweet.hasOwnProperty('timestamp_ms')) {
        res.date_ms = tweet.timestamp_ms
    } else if (tweet.hasOwnProperty('created_at')) {
        res.date_ms = new Date(tweet.created_at).getTime()
    }

    res.text = status.full_text
    return res
}

/**
 * Inits a new tweet by appending it to the DOM, taking care of syncing nlp and canvas.
 * Likewise, removes `offscreen` tweets out of DOM and canvas, however they're used there.
 */
const tweet_init = tweet => {
    tweet.state = TWEET_STATE.visible
    tweet.html = html.tweet_create(tweet)

    for (let offset_id of html.display_update()) {
        const offset_tweet = buffer_tweet(offset_id)
        try {
            offset_tweet.state = TWEET_STATE.offscreen
            offset_tweet.html.remove()
            delete offset_tweet.html
        }   // this may cause error
        catch (e) {
            console.warn(`can't remove ${offset_tweet.id} from DOM`)
        }
    }

    CLOCK.start = null
    CLOCK.timeout = Infinity
    CLOCK.cycle_count = Math.floor(document.querySelectorAll('div.tweet').length * .5)

    // canvas.refresh()
}

/** Main animation calllback. */
const on_frame = ts => {
    const tweets = Array.from(document.querySelectorAll(`div.tweet`)).reverse()
    const top_tweet = buffer_tweet(tweets[0].getAttribute('data-id'))

    if (top_tweet) {
        active_side = top_tweet.side

        if (CLOCK.start === null) {
            CLOCK.start = ts
            CLOCK.timeout = ts + top_tweet.text.length * LETTER_MS
        }

        if (CLOCK.timeout < ts) {
            const next_side = active_side === 'yin' ? 'yang' : 'yin'
            const next_tweets = buffer_sort(next_side).filter(t => t.state !== TWEET_STATE.visible && t.state !== TWEET_STATE.offscreen)

            if (next_tweets.length > 0) {
                const next_tweet = next_tweets[0]

                top_tweet.html.setAttribute('data-sibling', next_tweet.id)
                top_tweet.html.setAttribute('data-lev_distance', LevenshteinDistance(top_tweet.text, next_tweet.text))

                tweet_init(next_tweet)
                active_side = next_side
            } else if (next_tweets.length < MIN_COUPLE_BUFFER && queue_full) {
                if (!queue_renewing) {
                    queue_renewing = true
                    ipcRenderer.send('buffer_low')
                }
            }

        }

        // canvas.update(CLOCK)
    }

    window.requestAnimationFrame(on_frame)
}

ipcRenderer.on('tweet_couple',
    (e, tweets) => {
        for (let tweet of tweets) {
            const formatted_tweet = tweet_format(tweet)

            if (state === 'idle' && formatted_tweet.side === active_side) {
                state = 'running'

                tweet_init(formatted_tweet)
                window.requestAnimationFrame(on_frame)
            }

            buffer_append(formatted_tweet)
            history_append(formatted_tweet)

        }
    })

// received when the server buffer is full, used to replug socket when client is running on low buffer
ipcRenderer.on('queue_full', e => queue_full = true)
ipcRenderer.on('queue_renewed', e => queue_renewing = false)