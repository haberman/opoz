import { Stage, Layer, Group, Line, Circle } from 'konva'
import { GLOBAL_MARGIN } from './html'

let stage = null
let waves_layer = null

const WAVE_CYCLES = 12

const LINE_RESOLUTION = 256
const LINE_WEIGHT = 2
const LINE_ALPHA = .75

const LINE_STYLE = {
    yin: {
        fill: 'none',
        stroke: `rgba(60,15,200,${LINE_ALPHA})`,
        strokeWidth: LINE_WEIGHT
    },
    yang: {
        fill: 'none',
        stroke: `rgba(10,10,10,${LINE_ALPHA})`,
        strokeWidth: LINE_WEIGHT
    },
    special_red: {
        fill: `rgba(245,10,10,${LINE_ALPHA})`,
        stroke: `black`,
        strokeWidth: 1

    },
    special_green: {
        fill: `rgba(10,245,10,${LINE_ALPHA})`,
        stroke: `black`,
        strokeWidth: 1
    }
}

const create = () => {
    const stage_container = document.createElement('div')
    stage_container.setAttribute('id', 'background')
    document.body.insertBefore(stage_container, document.body.childNodes[0])

    const stage = new Stage({
        container: 'background',
        width: window.innerWidth,
        height: window.innerHeight
    })

    waves_layer = new Layer()
    stage.add(waves_layer)

    window.addEventListener('resize', () => {
        stage.setWidth(window.innerWidth)
        stage.setHeight(window.innerHeight)
    })

    return stage
}

export const update = ts => {
    return
    if (stage === null) { return }

    for (let wave of waves_layer.find('.wave')) {
        try {
            const tweet = document.querySelector(`div.tweet[data-id="${wave.id()}"] p`)
            const bbox_tweet = tweet.getBoundingClientRect()

            wave.y(bbox_tweet.bottom)
        } catch (e) { wave.destroy() }
    }

    stage.draw()
}

export const refresh = () => {
    return
    if (stage === null) { stage = create() }

    const tweets = Array.from(document.querySelectorAll(`div.tweet`))
    if (tweets.length < 2) { return }

    for (let i = 0; i < tweets.length; ++i) {
        const tweet = tweets[i]
        if (!tweet.hasAttribute('data-sibling')) { continue }  // tweet has no sibling yet

        const id_tweet = tweet.getAttribute('data-id')
        const on_stage = waves_layer.find(`#${id_tweet}`)
        if (on_stage.length > 0) { continue }                  // tweet is already on stage

        const id_sibling = tweet.getAttribute('data-sibling')
        const side_tweet = tweet.classList.contains('yin') ? 'yin' : 'yang'

        const bbox_tweet = tweet.querySelector('p').getBoundingClientRect()
        const bbox_sibling = document.querySelector(`div.tweet[data-id="${id_sibling}"] p`).getBoundingClientRect()

        const wave = new Group({ id: id_tweet, name: 'wave' })
        waves_layer.add(wave)

        const stage_width = stage.getWidth()
        const cycles = Math.PI * WAVE_CYCLES
        const inc = 1 / LINE_RESOLUTION
        const lev_proximity = 1 - Math.min(tweet.getAttribute('data-lev_distance'), 256) / 256

        let r = 0
        for (let w = 0; w <= 1; w += inc) {
            const ease = Math.sin(Math.PI * w) * lev_proximity

            const x = w * stage_width
            const y_top = Math.sin(r) * ease * bbox_tweet.height
            const y_bottom = Math.sin(r - Math.PI) * ease * bbox_sibling.height

            const line = new Line(Object.assign(LINE_STYLE[side_tweet],
                { points: [x, y_top, x, y_bottom] }))

            wave.add(line)
            r += cycles * inc
        }

        wave.offsetY(-2 * GLOBAL_MARGIN)
        wave.y(bbox_tweet.bottom)
    }
}