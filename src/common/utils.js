import fs from 'fs'
import path from 'path'

export const PATHS = {
    rate_limits: path.join(__static, 'rate_limits.json'),
    tweets_history: path.join(__static, 'tweets_history.json')
}

export const number_ease = (min, max, cursor) => {
    // t: current time, b: begInnIng value, c: change In value, d: duration
    // -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;

    return min + cursor * max
}

export const json_overwrite = (path, content) => {
    const json = JSON.stringify(content)
    return fs.writeFileSync(path, json)
}

export const json_read = (path, init_with) => {
    if (!fs.existsSync(path)) {
        if (init_with !== undefined) { fs.writeFileSync(path, init_with) }
        else { return null }
    }

    const data = fs.readFileSync(path)
    return JSON.parse(data)
}

export const tweet_is_unique = (tweet, list) => !list.map(t => t.id).includes(tweet.id)

export const history_append = tweet => {
    let history = []
    history = json_read(PATHS.tweets_history)
    if (history === null) { history = json_read(PATHS.tweets_history, '[]') }

    if (tweet_is_unique(tweet, history)) {
        const light_tweet = {
            id: tweet.id, date_ms: tweet.date_ms, type: tweet.type, text: tweet.text, side: tweet.side
        }

        history.push(light_tweet)
        return json_overwrite(PATHS.tweets_history, history)
    } else { return history }
}

/** Returns an array of rates' data for statuses where `remaining` < `limit` */
export const resources_used = () => {
    const resources = json_read(PATHS.rate_limits, '{}').resources
    const used = []

    Object.keys(resources).forEach(family => {
        Object.keys(resources[family]).forEach(endpoint => {
            const rate = resources[family][endpoint]
            if (rate.limit > rate.remaining) { used.push(Object.assign(rate, { endpoint })) }
        })
    })

    return used
}