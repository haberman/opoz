export const TWEET_STATE = {
    idle: 'TWEET_STATE_IDLE',                // when received from Twitter
    visible: 'TWEET_STATE_VISIBLE',          // when displayed
    offscreen: 'TWEET_STATE_OFFSCREEN'
}