import path from 'path'
import { format as format_url } from 'url'
import { app, ipcMain, globalShortcut, BrowserWindow } from 'electron'

import * as twitter from './twitter'

const IS_DEV = process.env.NODE_ENV !== 'production'
const LANG = 'fr'

// Prevent window from being garbage collected
let main_window = null

/** Creates a frameless window, loads the renderer and starts searching tweets. */
const create = () => {
  if (main_window !== null) { return }

  main_window = new BrowserWindow({ fullscreen: true })
  main_window.on('closed', () => { main_window = null })

  if (IS_DEV) {
    main_window.webContents.openDevTools()
    main_window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`)
  } else {
    main_window.loadURL(format_url({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true
    }))
  }

  ipcMain.on('buffer_low', e => {
    console.log('buffer low')
    twitter.search_start(LANG)
  })

  globalShortcut.register('Escape', quit)

  twitter.connect(
    LANG,
    tweet_couple => main_window.webContents.send('tweet_couple', tweet_couple),
    queue_state => main_window.webContents.send(queue_state))
}

/** Closes the `BrowserWindow` application. */
const quit = () => { app.quit() }

app.on('window-all-closed', () => { if (process.platform !== 'darwin') { quit() } })
app.on('ready', create)
app.on('activate', create)