export const QUERIES = [
    {
        value: 'paix',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'calme',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'tranquillité',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'sérénité',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'accord',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'repos',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'entente',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'union',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'ordre',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'sécurité',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'quiétude',
        language: 'fr',
        side: 'yin'
    },
    {
        value: 'guerre',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'dispute',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'querelle',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'combat',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'bataille',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'lutte',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'attaque',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'offensive',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'opposition',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'révolution',
        language: 'fr',
        side: 'yang'
    },
    {
        value: 'peace',
        language: 'en',
        side: 'yin'
    },
    {
        value: 'calm',
        language: 'en',
        side: 'yin'
    },
    {
        value: 'tranquility',
        language: 'en',
        side: 'yin'
    },
    {
        value: 'serenity',
        language: 'en',
        side: 'yin'
    },
    {
        value: 'war',
        language: 'en',
        side: 'yang'
    },
    {
        value: 'dispute',
        language: 'en',
        side: 'yang'
    },
    {
        value: 'fight',
        language: 'en',
        side: 'yang'
    },
    {
        value: 'battle',
        language: 'en',
        side: 'yang'
    },
    {
        value: 'vrede',
        language: 'nl',
        side: 'yin'
    },
    {
        value: 'frieden',
        language: 'de',
        side: 'yin'
    },
    {
        value: 'oorlog',
        language: 'nl',
        side: 'yang'
    },
    {
        value: 'krieg',
        language: 'de',
        side: 'yang'
    }]