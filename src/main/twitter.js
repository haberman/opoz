import Twit from 'twit'
import log from 'electron-log'

import { resources_used, json_read, json_overwrite, PATHS } from '../common/utils'
import { TWITTER_CREDENTIALS } from '../common/private'
import { TWEET_STATE } from '../common/states'
import { QUERIES } from './queries'

const SEARCH_COUNT = 30
const TWEET_MODE = { tweet_mode: 'extended' }

let twit = new Twit(Object.assign(TWITTER_CREDENTIALS, { timeout_ms: 30 * 1000 }))
let feed_couple = { yin: null, yang: null }
let couple_callback, queue_callback

/** Function to watch `rate_limit_status` that will called itself on the latest `reset` timestamp. */
const rate_fetch = async () => {
    try {
        const rate_statuses = await twit.get('application/rate_limit_status')
        json_overwrite(PATHS.rate_limits, rate_statuses.data)

        const used_resources = resources_used()
        log.info(`used resources:\n${JSON.stringify(used_resources)}`)
        return true
    } catch (err) {
        log.error(`rate_watch error: ${err.stack}`)
        return false
    }
}

/** Returns the next endpoint's reset timestamp for the connected Twitter credentials. */
const rate_next_reset = async (group, endpoint) => {
    let ms = 0

    try {
        const rate_limits = await json_read(PATHS.rate_limits)
        ms = rate_limits.resources[group][endpoint].reset
    } catch (err) { log.error(`rate_next_reset error: ${err.stack}`) }

    return ms
}

/** Appends a `since` *today* parameter for `terms` of a search request. */
const search_today = async (terms, lang, count) => {
    const d = new Date()
    const year = d.getFullYear()

    let month = '' + (d.getMonth() + 1),
        day = '' + d.getDate()

    if (month.length < 2) { month = `0${month}` }
    if (day.length < 2) { day = `0${day}` }

    let q = terms.reduce((acc, curr) => `${acc} OR ${curr}`)
    q += ` since ${[year, month, day].join('-')}`

    let res = []
    try {
        res = await twit.get('search/tweets', Object.assign(TWEET_MODE, { q, lang, count }))
        return res.data
    } catch (err) { res = await search_failed(err) }

    return res
}

/** Geats tweets of recent `result_type` for the given terms.  */
const search_recent = async (terms, lang, count) => {
    const q = terms.reduce((acc, curr) => `${acc} OR ${curr}`)

    let res = []
    try {
        res = await twit.get('search/tweets', Object.assign(TWEET_MODE, { q, lang, count }))
        res = res.data
    } catch (err) { res = await search_failed(err) }

    return res
}

/** Common exit on failure functin where we should gently wait for next reset to retry.  */
const search_failed = async error => {
    const timeout_ms = await rate_next_reset('search', '/search/tweets')
    log.error(`-*twitter*-`
        + `error while searching recent tweets\n${error.stack}\n`
        + `Programming next reset on ${new Date(timeout_ms).toDateString()}`)

    return []
}

/** Filters the `feed_couple` side's tweets by comparing values whose keys match `args` ones. */
const queue_filter = (side, args) => {
    if (feed_couple[side] === null) { return [] }
    let tweets = feed_couple[side]
    for (let key in args) { tweets = tweets.filter(t => t[key] === args[key]) }

    return tweets
}

/** Adds the tweet to the its couple's side. Returns false if the `feed` length surpasses `QUEUE_BY_SIDE`. */
const queue_add_tweet = (feed, tweet, side, tracks) => {
    if (feed.length >= SEARCH_COUNT) { return false }

    tweet.state = TWEET_STATE.idle
    Object.assign(tweet, { side, tracks })
    feed.push(tweet)

    return true
}

/** Picks a random item in the `feed_couple` side's tweets with eventual filtering. */
const queue_random_tweet = (side, filter) => {
    const candidates = queue_filter(side, filter)
    if (candidates.length === 0) { return null }

    return candidates[Math.floor(Math.random() * candidates.length)]
}


/** Tries to add incoming tweet in queue and invoke the `couple_callback` function in case of succes.  */
const queue_transport_tweet = (tweet, side, tracks) => {
    if (queue_add_tweet(feed_couple[side], tweet, side, tracks)) {
        const tweets_couple = queue_tweets_couple()
        if (tweets_couple === null) { return false }
        if (couple_callback) { couple_callback(tweets_couple) }
    }
}

/** Returns a random tweet couple or null in case of failure. */
const queue_tweets_couple = () => {
    const yin_tweet = queue_random_tweet('yin', { state: TWEET_STATE.idle })
    const yang_tweet = queue_random_tweet('yang', { state: TWEET_STATE.idle })

    if (yin_tweet === null || yang_tweet === null) { return null }
    return [yin_tweet, yang_tweet]
}

/** Starts a new search for the given language. */
export const search_start = async language => {
    feed_couple = { yin: null, yang: null }

    let tweet_found = 0
    for (let side of Object.keys(feed_couple)) {
        feed_couple[side] = []

        const tracks = QUERIES
            .filter(q => q.language === language && q.side === side)
            .map(q => q.value)

        const recent_res = await search_recent(tracks, language, SEARCH_COUNT)
        recent_res.statuses.forEach(
            status => {
                if (queue_transport_tweet(status, side, tracks)) { ++tweet_found }
            })

        if (tweet_found < SEARCH_COUNT) {
            const today_res = await search_today(tracks, language, SEARCH_COUNT - tweet_found)
            today_res.statuses.forEach(status => queue_transport_tweet(status, side, tracks))
        }
    }

    queue_callback('queue_renewed')
    return true
}

/** Authentificates applicaiton credentials against Twitter. Returns a `feed_couple` object. */
export const connect = async (language, on_couple, on_queue) => {
    await rate_fetch()

    couple_callback = on_couple
    queue_callback = on_queue

    const queue_full = await search_start(language)
    if (queue_full) { queue_callback('queue_full') }

    return feed_couple
}